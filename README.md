# Bazy Danych - MS Access


<img src="https://gitlab.com/pansmutku/access-database/raw/master/database.png" title="Bazy Danych" alt="Bazy Danych" width="100%" height="100%">

## Różne relacje

<strong>W projekcie zostały zastosowane różnego rodzaju relacje.</strong> 


<i>Projekt stworzył: Marcin Woźniak</i>

#### Proces tworzenia relacji:

1. Klikamy prawym przyciskiem myszy na wolne pole, następnie wybieramy <b><i>"Pokaż tabelę"</b></i>
<img src="https://gitlab.com/pansmutku/access-database/raw/master/db1.png" title="Bazy Danych" alt="Bazy Danych" width="20%" height="20%">

2. Z menu wybieramy tabele, które nas interesują. [<i>Do których chcemy dodać odpowiednie relacje </i>]
<img src="https://gitlab.com/pansmutku/access-database/raw/master/db2.png" title="Bazy Danych" alt="Bazy Danych" width="30%" height="40%">

3. Przy pomocy kreatora, wybieramy, które komórki mamy zamiar połączyć.
<img src="https://gitlab.com/pansmutku/access-database/raw/master/db3.png" title="Bazy Danych" alt="Bazy Danych" width="40%" height="40%">

4. Po kliknięciu w "<b><i>Typ sprzężenia</b></i>" jesteśmy w stanie wybrać typ połączenia, taki jaki nam odpowiada. 
<img src="https://gitlab.com/pansmutku/access-database/raw/master/db4.png" title="Bazy Danych" alt="Bazy Danych" width="40%" height="40%">


#### License 
[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
  
